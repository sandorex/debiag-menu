#!/bin/bash
VERSION="0.1"
DEV_STATE="Alpha"
NAME="debiag"
BACKTITLE="[debiag-info $VERSION $DEV_STATE]" # --no-tags --no-mouse
DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)"
ASSETS="$DIR/assets"
SCRIPTS="$DIR/scripts"
TEMP_MOUNT="/mnt/$NAME/temp"

if [[ $EUID -ne 0 ]]
then
   echo "This script must be run as root" 1>&2
   exit 1
fi

export NAME
export VERSION
export DIR
export ASSETS
export SCRIPTS

source $SCRIPTS/functions.sh

#TODO go to ram on boot so i can unplug usb/phone

runtime
