#!/bin/bash

function err
{
    echo -e "\e[31m[ERROR] $1\e[0m"
}

function uf
{
    whiptail --backtitle "$BACKTITLE" --title "ERROR" --msgbox "unimplemented function" 15 50
    menu
}

function generate_hardware_info
{
    mkdir -p $DIR/temp

    lshw -c "SYSTEM" > $DIR/temp/system
    lshw -c "CPU" > $DIR/temp/cpu
    lshw -c "DISPLAY" > $DIR/temp/display
    lshw -c "MEMORY" > $DIR/temp/memory
}

function get_hardware_info
{
    #lshw > $DIR/temp/full

    ARCH=$($SCRIPTS/hardware/hardware.sh cpu arch)
    CPU=$($SCRIPTS/hardware/hardware.sh cpu)
    GPU=$($SCRIPTS/hardware/hardware.sh gpu)
    MOBO_SHORT=$($SCRIPTS/hardware/hardware.sh mobo short)
    MOBO=$($SCRIPTS/hardware/hardware.sh mobo)
    MEM=$($SCRIPTS/hardware/hardware.sh memory)
    MEM_TOTAL=$($SCRIPTS/hardware/hardware.sh memory total)
    #BIOS_SHORT=$($SCRIPTS/hardware/hardware.sh bios short) not needed currently
    BIOS=$($SCRIPTS/hardware/hardware.sh bios)
    HDD_SHORT=$($SCRIPTS/hardware/hdd-info.sh short)
    HDD=$($SCRIPTS/hardware/hdd-info.sh)
}

function clean_hardware_info
{
    rm -rf $DIR/temp/
}

function full_diag # FULL SPECS
{
    get_hardware_info

    echo -e "ARCH: $ARCH"
    echo -e "BIOS:\n$BIOS"
    echo -e "PC:\n$MOBO"
    echo -e "CPU:\n$CPU"
    echo -e "GPU:\n$GPU"
    echo -e "RAM:\n$MEM"
    echo -e "HDD:\n$HDD"
}

function diag # GENERAL
{
    echo -e "\e[33m"
    figlet "$NAME"
    echo -e "\e[0m"

    get_hardware_info

    echo -e "\e[1m\e[34mARCH: $ARCH"
    echo -e "PC: $MOBO_SHORT"
    echo -e "CPU:$(echo "$CPU" | head -n 1)"
    echo -e "GPU:$(echo "$GPU" | head -n 1)"
    echo -e "RAM: $MEM_TOTAL [$(echo "$MEM" | grep -iwc "DIMM[0-9]") Memory Sticks]"
    echo -e "HDD:\n$(echo -e "$HDD_SHORT")\e[0m"
}

function internet_check
{
    echo "Checking for internet connectiong please wait..."

    if [[ "$(ping google.com -c 1 &>/dev/null && echo true)" == "true" ]]
    then
        whiptail --backtitle "$BACKTITLE" --title "SUCCESS" --msgbox "This computer has internet access" 15 50
    else
        whiptail --backtitle "$BACKTITLE" --title "ERROR" --msgbox "This computer does not have internet access" 15 50
    fi
    diag_menu
}

function save_to_file
{
    function getd
    {
        echo $DRIVES | awk "{split(\$0,a,\" \"); print a[$1]}"
    }

    ARG=""
    DRIVES=$(sudo fdisk -l | perl -e '/.*(\/dev\/.*).*/ && print $1 . "\n"' -n | perl -e '/(\/dev\/[a-zA-Z0-9]+) .*/ && print $1 . "\n"' -n)
    for d in $(seq $(echo $DRIVES | wc -w))
    do
        ARG+="$d $(getd $d) "
    done

    x=$(whiptail --nocancel --backtitle "$BACKTITLE" --notags --title "debiag-menu: Diagnostics" --menu "Please choose an option" 15 50 8 \
            $ARG        \
            0 "Back"  \
            3>&1 1>&2 2>&3)

    if [[ ! $? ]]
    then
        err "error showing whiptail"
        exit 1
    fi

    clear

    if [[ "$x" == "0" ]]
    then
        diag_menu
        return
    fi

    selected="$(getd $x)"

    if [[ ! selected ]]
    then
        whiptail --backtitle "$BACKTITLE" --title "debiag-menu: Diagnostics" --msgbox "File has not been saved" 10 40
        diag_menu
    fi

    mkdir -p "$TEMP_MOUNT"

    if mountpoint "$TEMP_MOUNT" &>/dev/null
    then
        whiptail --backtitle "$BACKTITLE" --title "ERROR" --msgbox "Error temp ($TEMP_MOUNT) is already mounted" 10 40
        diag_menu
        return
    fi

    echo -e "\e[33m\e[1mMounting \"$selected\"...\e[0m"

    if ! mount | grep "$selected" -q
    then
        if ! mount "$selected" "$TEMP_MOUNT" &>/dev/null
        then
            whiptail --backtitle "$BACKTITLE" --title "debiag-menu: Diagnostics" --msgbox "Could not mount $selected filesystem" 10 40
            save_to_file
            return
        fi
    else
        whiptail --backtitle "$BACKTITLE" --title "ERROR" --msgbox "Error $selected is already mounted" 10 40
        diag_menu
        return
    fi

    echo -e "\e[33m\e[1mWriting to file...\e[0m"

    if [[ -f "$TEMP_MOUNT/debiag.txt" ]]
    then
        mv -f "$TEMP_MOUNT/debiag.txt" "$TEMP_MOUNT/debiag.txt.old"
    fi

    echo -e "$(full_diag)\n\n$(date)" > "$TEMP_MOUNT/debiag.txt"
    whiptail --backtitle "$BACKTITLE" --title "debiag-menu: Diagnostics" --msgbox "File written in root of $selected" 10 40
    sudo umount "$TEMP_MOUNT"
    diag_menu
}

# GUI SECTION

function diag_menu
{
    echo -e "\e[33m\e[1mGetting hardware info...\e[0m"

    x=$(whiptail --nocancel --backtitle "$BACKTITLE" --notags --title "debiag-menu: Diagnostics" --menu "Please choose an option" 15 50 8 \
            1   "General"               \
            8   "Partitions"            \
            2   "Storage Drives"        \
            7   "Full Specifications"   \
            3   "Temperatures"          \
            4   "Internet Check"        \
            5   "WIFI"                  \
            6   "Save Full Specs"       \
            99  "Back"                  \
            3>&1 1>&2 2>&3)

    if [[ ! $? ]]
    then
        err "error showing whiptail"
    fi

    clear

    case $x in
        1 )
            diag
            read
            diag_menu
            ;;
        2 )
            uf
            ;;
        3 )
            uf
            ;;
        4 )
            internet_check
            ;;
        5 )
            uf
            ;;
        6 )
            save_to_file
            ;;
        7 )
            echo -e "\e[33m\e[1mLoading hardware info\e[0m"
            whiptail --scrolltext --backtitle "$BACKTITLE" --title "debiag-menu: Full Specifications" --msgbox "$(full_diag)" 20 60
            diag_menu
            ;;
        8 )
            uf
            ;;
        99 )
            menu
            ;;
        * )
            menu
            ;;
    esac
}

function tricks_menu
{
    whiptail --backtitle "$BACKTITLE" --title "WARNING" --msgbox "ADVANCED USER OONLY\nUSE WITH CARE!" 10 60
    x=$(whiptail --nocancel --backtitle "$BACKTITLE" --notags --title "debiag-menu: Tricks" --menu "Please choose an option" 15 50 8 \
            1   "cfdisk"        \
            2   "mkfs"          \
            99  "Back"          \
            3>&1 1>&2 2>&3)

    if [[ ! $? ]]
    then
        err "error showing whiptail"
        exit 1
    fi

    clear

    case $x in
        1 )
            cfdisk
            tricks_menu
            ;;
        99 )
            menu
            ;;
        * )
            menu
            ;;
    esac
}

function about
{
    echo -e "\e[33m"
    figlet "SandoreX"
    echo
    echo -e "debiag-menu Copyright (C) 2016  Aleksandar Radivojevic <rzhw3h@gmail.com>
This program comes with ABSOLUTELY NO WARRANTY
This is free software, and you are welcome to redistribute it under certain conditions"
    echo -e "\e[0m"
    read
    menu
}

function menu
{
    x=$(whiptail --nocancel --backtitle "$BACKTITLE" --notags --title "debiag-menu" --menu "Please choose an option" 15 50 6 \
            1   "Diagnostics"           \
            2   "Advanced Utilities"    \
            3   "Shell"                 \
            4   "Settings"              \
            5   "About"                 \
            99  "Power"                 \
            3>&1 1>&2 2>&3)

    if [[ ! $? ]]
    then
        err "error showing whiptail"
        exit 1
    fi

    clear

    case $x in
        1 )
            diag_menu
            ;;
        2 )
            tricks_menu
            ;;
        3 )
            whiptail --backtitle "$BACKTITLE" --title "WARNING" --msgbox "ADVANCED USER ONLY\nUSE WITH CARE!" 10 60
            clear
            /bin/bash --init-file $SCRIPTS/bashrc
            menu
            ;;
        4 )
            uf
            ;;
        5 )
            about
            ;;
        99 )
            power_menu
            ;;
        * )
            exit 1
            ;;
    esac
}

function power_menu
{
    x=$(whiptail --nocancel --backtitle "$BACKTITLE" --notags --title "debiag-menu" --menu "Please choose an option" 15 50 3 \
            1   "Shutdown"  \
            2   "Restart"   \
            99   "Back"     \
            3>&1 1>&2 2>&3)

    if [[ ! $? ]]
    then
        err "error showing whiptail"
        exit 1
    fi

    clear

    case $x in
        1 )
            #clean_hardware_info
            echo -e "\e[32mshutdown\e[0m"
            ;;
        2 )
            #clean_hardware_info
            echo -e "\e[32mrestart\e[0m"
            ;;
        99 )
            menu
            ;;
        * )
            clean_hardware_info
            exit 1
            ;;
    esac
}

function runtime
{
    if [[ ! -d $DIR/temp ]]
    then
        echo -e "\e[33m\e[1mGetting hardware information please wait...\e[0m"
        generate_hardware_info
    fi
    menu
}
