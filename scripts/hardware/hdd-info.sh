#!/bin/bash
function HDD
{
    cat $DIR/temp/hdd | awk "NR==$1{print}"
}
mkdir -p $DIR/temp
$DIR/scripts/hardware/HDSentinel -solid > $DIR/temp/hdd
HDDC=$(cat $DIR/temp/hdd | wc -l)
for ((x=1;x<=$HDDC;x++))
do
    file=$(echo -e "$(HDD $x)" | awk '{split($0,a," "); print a[1]}')
    model=$(echo -e "$(HDD $x)" | awk '{split($0,a," "); print a[5]}')
    temperature=$(echo -e "$(HDD $x)" | awk '{split($0,a," "); print a[2]}')
    health=$(echo -e "$(HDD $x)" | awk '{split($0,a," "); print a[3]}')
    size_mb=$(echo -e "$(HDD $x)" | awk '{split($0,a," "); print a[7]}')
    size_gb=$(echo -e "$(HDD $x)" | awk '{split($0,a," "); print a[7]/1024}')


    if [[ "$1" == "short" ]]
    then
        echo -e " $file $temperature°C $health% $model ${size_gb}GB (${size_mb}MB)"
        #echo " $(echo -e "$(HDD $x)" | awk '{split($0,a," "); print a[1]," ",a[2],"°C HP: ",a[3],"% M: ",a[5]," ",a[7]/1024,"GB (",a[7],"MB)"}' | xargs)"
    else
        echo -e " Name: $file\n Model: $model\n Temperature: $temperature°C\n Health: $health%\n Size: ${size_gb}GB (${size_mb}MB)\n"
    fi
done
