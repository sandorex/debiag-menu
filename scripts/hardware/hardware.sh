#!/bin/bash

HW="$DIR/temp"

function get_cpu_info
{
    FILE="$HW/cpu"
    model=$(cat $FILE | xargs | perl -e '/.*product: (.*) vendor:.*slot: (.*).*size:.*capacity: (.*).*width:.*/ && print $1' -n | xargs)
    socket=$(cat $FILE | xargs | perl -e '/.*product: (.*) vendor:.*slot: (.*).*size:.*capacity: (.*).*width:.*/ && print $2' -n | xargs)
    max_freq=$(cat $FILE | xargs | perl -e '/.*product: (.*) vendor:.*slot: (.*).*size:.*capacity: (.*).*width:.*/ && print $3' -n | xargs)
    arch=$(lscpu | head -n 1 | xargs | perl -e '/.*: (.*)/ && print $1' -n | xargs) # TODO read from cpu file too


    if [[ "$1" == "arch" ]]
    then
        echo -e "$arch"
        return
    fi

    echo -e " $model\n Max Clock: $max_freq\n $socket"
}

function get_gpu_info
{
    FILE="$HW/display"
    model=$(cat $FILE | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id:.*/ && print $2' -n)
    vendor=$(cat $FILE | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id:.*/ && print $3' -n)
    desc=$(cat $FILE | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id:.*/ && print $1' -n)

    echo -e " $model\n $vendor\n $desc"
}

function get_motherboard_info
{
    FILE="$HW/system"
    pc_type=$(cat $FILE | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) version: (.*) serial:.*/ && print $1' -n)
    model=$(cat $FILE | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) version: (.*) serial:.*/ && print $2' -n)
    vendor=$(cat $FILE | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) version: (.*) serial:.*/ && print $3' -n)
    version=$(cat $FILE | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) version: (.*) serial:.*/ && print $4' -n)

    if [[ "$1" == "short" ]]
    then
        echo -e "$pc_type $model $version ($vendor)"
    else
        echo -e " Type: $pc_type\n Model: $model\n Version: $version\n Vendor: $vendor"
    fi
}

function get_memory_info
{

    FILE="$HW/memory" # cat temp/memory | sed -n -e '/TERMINATE/,$p' # sed -e '/TERMINATE/,$d'
    slot_count=$(cat temp/memory | sed -n -e '/*-memory/,$p' | grep "*-bank" | wc -l)
    total_ram=$(cat temp/memory | sed -n -e '/*-memory/,$p' | head -n 5 | xargs | perl -e '/.*size: (.*).*/ && print $1 . "\n"' -n)

    if [[ "$1" == "total" ]]
    then
        echo -e "$total_ram"
        return
    fi

    echo -e " Total: $total_ram\n"

    for s in $(seq 0 $(($slot_count - 1)))
    do
        x=$(cat $FILE | sed -n -e "/*-bank:$s/,\$p" | sed -e "/*-bank:$(($s + 1))/,\$d")
        if ! echo "$x" | head -n 2 | grep "empty" &>/dev/null
        then
            model=$(echo "$x" | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id: .* slot: (.*) size: (.*) width:.* clock: (.*).*/ && print $2' -n)
            slot=$(echo "$x" | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id: .* slot: (.*) size: (.*) width:.* clock: (.*).*/ && print $4' -n)
            vendor=$(echo "$x" | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id: .* slot: (.*) size: (.*) width:.* clock: (.*).*/ && print $3' -n)
            size=$(echo "$x" | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id: .* slot: (.*) size: (.*) width:.* clock: (.*).*/ && print $5' -n)
            desc=$(echo "$x" | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id: .* slot: (.*) size: (.*) width:.* clock: (.*).*/ && print $1' -n)
            clock=$(echo "$x" | xargs | perl -e '/.*description: (.*) product: (.*) vendor: (.*) physical id: .* slot: (.*) size: (.*) width:.* clock: (.*).*/ && print $6' -n)

            echo -e " Slot: $slot\n Model: $model\n Vendor: $vendor\n Size: $size\n Clock: $clock\n Description: $desc\n"
            #echo -e "$slot $model [$vendor] $size {$clock, $desc}"
        fi
    done
}

function get_bios_info
{
    FILE="$HW/memory"
    desc=$(head -n 9 temp/memory | xargs | perl -e '/.*description: (.*) vendor: (.*) physical id:.* version: (.*) date: (.*) size:.*/ && print $1' -n)
    version=$(head -n 9 temp/memory | xargs | perl -e '/.*description: (.*) vendor: (.*) physical id:.* version: (.*) date: (.*) size:.*/ && print $3' -n)
    vendor=$(head -n 9 temp/memory | xargs | perl -e '/.*description: (.*) vendor: (.*) physical id:.* version: (.*) date: (.*) size:.*/ && print $2' -n)
    date=$(head -n 9 temp/memory | xargs | perl -e '/.*description: (.*) vendor: (.*) physical id:.* version: (.*) date: (.*) size:.*/ && print $4' -n)

    if [[ "$1" == "short" ]]
    then
        echo -e "$desc $version ($vendor) [$date]"
    else
        echo -e " Type: $desc\n Version: $version\n Vendor: $vendor\n Date: $date"
    fi
}

if [[ ! $DIR ]]
then
    echo "has to be run from debiag-menu"
    exit 1
fi

if [[ ! $1 ]]
then
    echo "invalid option"
    exit 1
fi

query="$1"

shift

if [[ -d $HW ]]
then
    case $query in
        gpu )
            get_gpu_info $@
            ;;
        cpu )
            get_cpu_info $@
            ;;
        mobo )
            get_motherboard_info $@
            ;;
        memory )
            get_memory_info $@
            ;;
        bios )
            get_bios_info $@
            ;;
        all )
            get_cpu_info $1
            echo
            get_gpu_info $2
            echo
            get_motherboard_info $3
            echo
            get_memory_info $4
            echo
            get_bios_info $5
            ;;
        * )
            echo "invalid option \"$query\""
            exit 1
            ;;
    esac
    exit 0
else
    echo "hardware file does not exist at $HW"
    exit 1
fi
